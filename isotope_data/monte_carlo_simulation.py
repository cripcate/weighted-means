# "Monte Carlo Simulation for Isotope Data"

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# IMPORT DATAFRAME
csv_file = ("/home/nils/Documents/Dropbox/Uni/M.Sc.Geo/"
            "2. Semester (16)/Kosmochemie Stabiler Isotope/"
            "Laborpraktikum/Python Auswertung/isotope_data/isotope_data.csv")
df = pd.read_csv(csv_file)

# Crop Dataframe
df = df.drop(df.index[0:10])
df_relevant = df.drop(["Position", "Masse [mg]", "Ausebeute (TCD out) [%]",
                       "d17O (raw) [‰]", "Fehler d17O", "d18O (raw) [‰]2",
                       "Fehler d18O", "I(32)", "I(28)", "I(40)"], axis=1)
df_relevant.reset_index(drop=True, inplace=True)
print(df_relevant)

# GENERATE DATAPOINTS
# 1. Take values + y and y errors and assume a gaussian normal distribution
# 2. Generate one point each based on that

recursion_data = []
# Iterate over rows and generate x and y data:
for i in df_relevant.index:
    # Setting mean and Covariance for Data generation
    mean = [df_relevant.iat[i, 3], df_relevant.iat[i, 5]]
    covariance = [[-(df_relevant.iat[i, 4]) / 2, df_relevant.iat[i, 4] / 2],
                  [df_relevant.iat[i, 6] / 2, abs(df_relevant.iat[i, 6]) / 2]]

    # Data generation
    x, y = np.random.multivariate_normal(mean, covariance, 500).T
    x = pd.Series(x, name='x')
    y = pd.Series(y, name='y')
    generated_data = pd.concat([x, y], axis=1)
    recursion_data.append(generated_data)

# CREATE LINEAR FITS
fits = []

for n in range(500):
    x_data = np.zeros(8)
    y_data = np.zeros(8)

    for i, df in enumerate(recursion_data):
        x_data[i] = df.iat[n, 0]
        y_data[i] = df.iat[n, 1]

    fit = np.polyfit(x_data, y_data, 1)
    fit_fn = np.poly1d(fit)  # Function that takes x and estimates y fit_fn(x)
    fits.append(fit_fn)

print(len(fits))

# CALCULATE MEAN AND STDEV OF LINEAR FITS
# Arrays used to compute the mean recursion later)
mean_data = np.zeros([180, 2], dtype=float)
stdev_data = np.zeros([180, 2], dtype=float)

# Call every fit with every x value to get a series of y values for each x
for i, x in np.ndenumerate(np.arange(7, 16, 0.05)):

    y = np.asarray([p(x) for p in fits])
    xy_means = [x, np.average(y)]
    xy_stdevs = [x, np.std(y)]

    mean_data[i, ] = xy_means
    stdev_data[i, ] = xy_stdevs

mean_fit = np.polyfit(mean_data[:, 0], mean_data[:, 1], 1)
mean_fit_polyfunc = np.poly1d(mean_fit)
print(mean_fit)
np.savetxt("mean_fit.csv", mean_fit, delimiter=",")
np.savetxt("stdevs.csv", stdev_data, delimiter=",")

# VISUALIZATION
plt.figure(1)
plt.suptitle("Monte-Carlo-Simulated Linear-Regression for D'17O / dd'18O")
plt.ylabel("D'17O")
plt.xlabel("d'18O")

axes = plt.gca()
axes.set_xlim([7, 17])

# Magnetite
x = df_relevant["d'18O(raw)"].loc[df_relevant["Probenname"] == "Magnetit"]
y = df_relevant["D'17O (raw) [ppm]"].loc[df_relevant["Probenname"] == "Magnetit"]
plt.scatter(x, y, marker='o', s=7**2, linewidth=0, c='#4D4D4D')

# Quartz
x = df_relevant["d'18O(raw)"].loc[df_relevant["Probenname"] == "Quarz"]
y = df_relevant["D'17O (raw) [ppm]"].loc[df_relevant["Probenname"] == "Quarz"]
plt.scatter(x, y, marker='v', s=7**2, linewidth=0, c='#5DA5DA')

# Hedenbergite
x = df_relevant["d'18O(raw)"].loc[df_relevant["Probenname"] == "Hedenbergit"]
y = df_relevant["D'17O (raw) [ppm]"].loc[df_relevant["Probenname"] == "Hedenbergit"]
plt.scatter(x, y, marker='s', linewidth=0, s=7**2, c='#60BD68')

# Regression
x = np.linspace(0, 16, 100)
y = [mean_fit_polyfunc(i) for i in x]
plt.plot(x, y, linewidth=2, c='#4D4D4D')
