# Calculate the weighted mean for Mineral Data

import pandas as pd
import numpy as np

# Import the dataset from .csv file stated in the function name
# TODO: Use sys.argv for filepath, or let the user input it
def getdata(filename):
    df = pd.read_csv(filename)
    print("Dataset loaded:\n\n")
    return df

# Get the x,y data and x,y errors for weight:
def getrelevant(df):
    x = df["d'18O(raw)"]
    y = df["D'17O (raw) [ppm]"]
    x_err = df["error d'18O(raw)"]
    y_err = df["Fehler D'17O"]
    return pd.DataFrame([df["Probenname"], x, y, x_err, y_err]).T

# Calculate the weights from x and y errors:
# Working part-time solution
def weightmean(df, weights):
    return (df * weights).sum() /  weights.sum()

# MAIN Script
if __name__ == "__main__":
    print("Pandas version: {}".format(pd.__version__))
    df = getdata("/home/nils/Documents/Dropbox/Uni/M.Sc.Geo/2. Semester (16)/Kosmochemie Stabiler Isotope/Laborpraktikum/Python Auswertung/isotope_data/isotope_data.csv")
    df.set_index('Probenname').describe()
    print(df)
    weight_data = getrelevant(df)
    print(weight_data)

    # Calculating weighted means
    x_mean = df.groupby('Probenname').apply(lambda probe: weightmean(probe["d'18O(raw)"], 1/probe["error d'18O(raw)"]))
    y_mean =  df.groupby('Probenname').apply(lambda probe: weightmean(probe["D'17O (raw) [ppm]"], 1/probe["Fehler D'17O"]))
    weighted_means = pd.DataFrame([x_mean, y_mean], index=["δ'18O Weighted Mean","Δ'17O Weighted Mean"])

    # Calculating standard Errors
    x_std_errs = df.groupby('Probenname').apply(lambda probe: np.average(probe["error d'18O(raw)"]))
    y_std_errs = df.groupby('Probenname').apply(lambda probe: np.average(probe["Fehler D'17O"]))
    std_errs = pd.DataFrame([x_std_errs, y_std_errs], index=["δ'18O Std. Err.","Δ'17O Std. Err."])
    result = pd.concat([weighted_means, std_errs])
    print(result.T)
