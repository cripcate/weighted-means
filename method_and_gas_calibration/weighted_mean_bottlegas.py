# Calculate the weighted mean for the Bottlegas

import pandas as pd
import numpy as np
import sys

# Import the dataset from .csv file stated in the function name
# TODO: Use sys.argv for filepath, or let the user input it
def getdata(filename):
    df = pd.read_csv(filename, error_bad_lines=False)
    print("Dataset loaded:\n\n")
    return df

# Get the x,y data and x,y errors for weight:
# TODO: Simplify
def getrelevant(df):
    x = df["δ18O"]
    y = df["δ17O"]
    x_err = df["δ18O Error"]
    y_err = df["δ17O Error"]
    return pd.DataFrame([x, y, x_err, y_err]).T

# Calculate the weights from x and y errors:
ddef calcmean(df):
    x_mean = np.average(df["δ18O"], weights=1/df["δ18O Error"])
    y_mean = np.average(df["δ17O"], weights= 1/df["δ17O Error"])
    x_err = np.average(df["δ18O Error"])
    y_err = np.average(df["δ17O Error"])
    return pd.DataFrame([[x_mean, y_mean],[x_err, y_err]], columns=["Weighted Mean", "Standard Error"], index=["δ18O","δ17O"])

# MAIN Script
if __name__ == "__main__":
    print("Pandas version: {}".format(pd.__version__))
    df = getdata("/home/nils/Documents/Dropbox/Uni/M.Sc.Geo/2. Semester (16)/Kosmochemie Stabiler Isotope/Laborpraktikum/Python Auswertung/method_calibration.csv")
    print(df)
    weight_data = getrelevant(df)
    print(weight_data)
    weighted_means = calcmean(weight_data)
    print(weighted_means)
